TEXSRC = $(wildcard ./*.tex)

all: submit.pdf

submit.pdf: $(TEXSRC) 
	pdflatex submit
	pdflatex submit

clean: 
	\rm -f *.dvi *.aux *.ps *~ *.log *.blg *.bbl submit.pdf
